<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // dashboard 

    public function dashboard()
    {
    	return view('admin.dashboard');
    }

    public function countrySpecificData()
    {
    	return view('admin.country-data');
    }

    public function buyersData()
    {
    	return view('admin.buyers');
    }
}
