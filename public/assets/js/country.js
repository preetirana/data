$( document ).ready(function() {


// countries wise targated date



var country_target = document.getElementById('country-target').getContext("2d");

var countryTarget = new Chart(country_target, {
    type: 'horizontalBar',
    data: {
        labels: ["Australia", "France", "Russia", "Canada", "India","China","United States","Spain","Japan"],
        datasets: [{
            label: "Data",
            borderColor: "transparent",
            borderWidth: 1,
            fill: true,
            backgroundColor: "#63cfa5",
            hoverBackgroundColor: "#cf6363",
            data: [100,70, 50, 60, 80, 40,20, 20,20 ,0]
        }]
    },
    options: {
        animation: {
            easing: "easeOutQuart"
        },
        legend: {
            position: "bottom",
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "#000000",
                    fontStyle: "bold",
                    fontSize:8,
                    beginAtZero: true,
                    padding: 10,
                    //display: false - remove this and commenting to display: false
                },
                gridLines: {
                    drawTicks: false,
                    display: false,
                    color: "#e3e3e3",
                    zeroLineColor: "transparent"
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false,
                    color: "#e3e3e3",
                    zeroLineColor: "transparent"
                },
                ticks: {
                    padding: 0,
                    beginAtZero: true,
                    fontColor: "#000000",
                    fontStyle: "bold",
                    maxTicksLimit: 20,
                    fontSize:8,
                    //display: false - remove this and commenting to display: false
                }
            }]
        }
    }
});



//trends traget countries

Highcharts.chart('traget-countries', {

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    exporting: { enabled: false },

    yAxis: {
        title: {
            text: false
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        borderWidth: 0
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Argentina',
        color:'#fd7f35',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }, {
        name: 'China',
        color:'#4472c4',
        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 100
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});


// pie chart
Highcharts.chart('portchart', {
  chart: {
    type: 'pie',
    options3d: {
      enabled: true,
      alpha: 65,
      beta: 0
    }
  },
  exporting: { enabled: false },
  title: {
    text: ""
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      depth: 35,
      size:180,
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
    }
  },
  series: [{
    type: 'pie',
    name: 'Browser share',
    data: [
      ['Firefox', 45.0],
      ['IE', 26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari', 8.5],
      ['Opera', 6.2],
      ['Others', 0.7]
    ]
  }]
});
// end of pie chart


});