$( document ).ready(function() {


// countries wise targated date


Highcharts.chart('suppliers-origin', {

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    exporting: { enabled: false },

    yAxis: {
        title: {
            text: false
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        borderWidth: 0
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },

    series: [{
        showInLegend: false,
        name: 'Argentina',
        color:'#fd7f35',
        data: [40000, 50000, 60000, 50000, 97031, 119931, 137133, 154175]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 100
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});




//trends traget countries

Highcharts.chart('worldwide-trends', {

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    exporting: { enabled: false },

    yAxis: {
        title: {
            text: false
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        borderWidth: 0
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'Argentina',
        color:'#fd7f35',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }, {
        name: 'China',
        color:'#4472c4',
        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 100
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});




});