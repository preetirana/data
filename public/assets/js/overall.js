$( document ).ready(function() {




Chart.defaults.global.elements.line.fill = false;

// export over time data
var barChartData = {
  labels: ['2017-01', '2017-02', '2017-03', '2017-04','2017-05','2017-06','2017-08','2017-09','2017-10'],
  datasets: [{
    type: 'bar',
    label: '%',
    id: "y-axis-0",
    backgroundColor: "#ed7d31",
    data: [1000000, 2000000, 4000000, 5000000, 6000000, 7000000]
  },  {
    type: 'line',
    label: 'data',
    id: "y-axis-0",
    backgroundColor: "#4774c5",
    data: [1500000, 2600000, 4700000, 5800000,3000000,7000000,4000000,8000000]
  }]
};


var ctx = document.getElementById("over-time");
// allocate and initialize a chart
var ch = new Chart(ctx, {
  type: 'bar',
  data: barChartData,
  options: {
    title: {
      display: true,
      // text: "Chart.js Bar Chart - Stacked"
    },
    tooltips: {
      mode: 'label'
    },
    responsive: true,
    scales: {

      barThickness: 20,
      xAxes: [{
        stacked: true,
        ticks:{
            fontSize:8,
        }
      }],
      yAxes: [{
        stacked: true,
        position: "left",
        id: "y-axis-0",
        ticks:{
            fontSize:8,
        }
      }, {
        stacked: false,
        position: "right",
        id: "y-axis-1",
      }]
    }
  }
});

// end of export over time

// country wise data


var ctx = document.getElementById('country-wise').getContext("2d");

var barStroke = ctx.createLinearGradient(700, 0, 120, 0);
barStroke.addColorStop(0, 'rgba(0, 255, 188, 0.6)');
barStroke.addColorStop(1, 'rgba(0, 205, 194, 0.6)');

var barFill = ctx.createLinearGradient(700, 0, 120, 0);
barFill.addColorStop(0, "rgba(0, 255, 188, 0.6)");
barFill.addColorStop(1, "rgba(0, 205, 194, 0.6)");

var barFillHover = ctx.createLinearGradient(700, 0, 120, 0);
barFillHover.addColorStop(0, "rgba(0, 255, 188, 0.8)");
barFillHover.addColorStop(1, "rgba(0, 205, 194, 0.6)");

var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: ["Australia", "France", "Russia", "Canada", "India","China","United States","Spain","Japan"],
        datasets: [{
            label: "Data",
            borderColor: "transparent",
            borderWidth: 1,
            fill: true,
            backgroundColor: "#63cfa5",
            hoverBackgroundColor: "#cf6363",
            data: [100,70, 50, 60, 80, 40,20, 20,20 ,0]
        }]
    },
    options: {
        animation: {
            easing: "easeOutQuart"
        },
        legend: {
            position: "bottom",
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "#000000",
                    fontStyle: "bold",
                    fontSize:8,
                    beginAtZero: true,
                    padding: 10,
                    //display: false - remove this and commenting to display: false
                },
                gridLines: {
                    drawTicks: false,
                    display: false,
                    color: "#e3e3e3",
                    zeroLineColor: "transparent"
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false,
                    color: "#e3e3e3",
                    zeroLineColor: "transparent"
                },
                ticks: {
                    padding: 0,
                    beginAtZero: true,
                    fontColor: "#000000",
                    fontStyle: "bold",
                    maxTicksLimit: 20,
                    fontSize:8,
                    //display: false - remove this and commenting to display: false
                }
            }]
        }
    }
});



// pie chart
Highcharts.chart('portchart', {
  chart: {
    type: 'pie',
    options3d: {
      enabled: true,
      alpha: 65,
      beta: 0
    }
  },
  exporting: { enabled: false },
  title: {
    text: ""
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      depth: 35,
      size:180,
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
    }
  },
  series: [{
    type: 'pie',
    name: 'Browser share',
    data: [
      ['Firefox', 45.0],
      ['IE', 26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari', 8.5],
      ['Opera', 6.2],
      ['Others', 0.7]
    ]
  }]
});
// end of pie chart





});