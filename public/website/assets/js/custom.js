$( document ).ready(function() {
  var shrinkHeader = 200;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('.navbar').addClass('shrink');
           $('.navbar-brand img').attr('src','/website/assets/images/white-logo.png');
        }
        else {
            $('.navbar').removeClass('shrink');
            $('.navbar-brand img').attr('src','/website/assets/images/logo.png');
        }
  });
  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }  

   

    // ===== Scroll to Top ==== 
   

    var btn = $('.go-to-top');

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').function({scrollTop:0}, '300');
    });
});

