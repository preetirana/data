<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dashboard</title>
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/css/bootstrap-dropdownhover.min.css">
	<!-- <link rel="stylesheet" href="/assets/css/w3.css"> -->
	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="stylesheet" href="/assets/css/responsive.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>
	<div class="header">
		<h4>Header</h4>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 top-padding">
				<div class="sidebar">
					<div class="top">
						<span>Country of origin:</span><br>
						<div class="select">
							<select name="slct" id="slct">
							    <option value="1">India</option>
							    <option value="2">USA</option>
							    <option value="3">Canada</option>
							 </select>	
						</div>
						<h4 class="heading">HS Code/Product description</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
						
					</div>
					<div class="top">
						<h4 class="heading">Commodities</h4>
						<div class="select border-bottom">
							<select name="slct" id="slct">
							    <option value="1">India</option>
							    <option value="2">USA</option>
							    <option value="3">Canada</option>
							 </select>		
						</div>
					</div>
					<div class="top side">
						<h4 class="heading">Financial Year</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>

					<div class="top side">
						<h4 class="heading">Month</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>
					<div class="top side">
						<h4 class="heading">Region</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>
					<div class="top side">
						<h4 class="heading">Port</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>
					<div class="top side">
						<h4 class="heading">Code</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>
					<div class="top side">
						<h4 class="heading">Name</h4>
						<div class="searchboxwrapper">
							<input class="searchbox" type="text" value="" name="s"  id="s">
							<input class="searchsubmit" type="submit" id="searchsubmit" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				@yield('main-content')
			</div>
		</div>
	</div>


	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/bootstrap-dropdownhover.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-3d.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="/assets/js/moment.min.js"></script>
	<script src="/assets/js/chart.min.js"></script>
	<script src="/assets/js/utils.js"></script>
	<script src="/assets/js/core.js"></script>
	<script src="/assets/js/charts.js"></script>
	<script src="/assets/js/animated.js"></script>
	<script src="/assets/js/app.js"></script>

	@yield('scripts')

</body>
</html>