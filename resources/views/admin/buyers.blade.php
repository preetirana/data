@extends('app')
@section('main-content')
<div class="tabs">
  <ul class="tabs-nav">
    <li><a href="/">Overall data</a></li>
    <li><a href="/country-specific-data">Country Specific data</a></li>
    <li><a href="#">Commodityspecific data</a></li>
    <li  class="tab-active"><a href="/buyers-data">Buyers Insights</a></li>
  </ul>
  <div class="tabs-stage">
  	<!-- select target data -->
  	<div class="target-data">
  		<h4 class="heading">Select Commodities</h4>
		<div class="searchboxwrapper">
			<input class="searchbox" type="text" value="" name="s"  id="s">
			<input class="searchsubmit" type="submit" id="searchsubmit" value="">
		</div>
  	</div>
    <!-- export chart -->
    <div class="export-chart">
    	<div class="col-md-6 top-padding">
    		<h5>Suppliers Trends</h5>
    		<div class="table-trends">
    				<table class="bordered">
					  <thead>
					    <tr>
					      <th>Customer NAme</th>
					      <th>Values(in INR)</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>James</td>
					      <td>5456</td>
					    </tr>
					    <tr>
					      <td>Andor</td>
					      <td>54654</td>
					    </tr>
					    <tr>
					      <td>Tamas</td>
					      <td>2134</td>
					    </tr>
					    <tr>
					      <td>Zoli</td>
					      <td>2132</td>
					    </tr>
					    <tr>
					      <td>Szabi</td>
					      <td>12134</td>
					    </tr>
					  </tbody>
					</table>
    			</div>
    		
    	</div>
    	<div class="col-md-6 top-padding">
    		<h5>Buyers Trends Worldwide</h5>
    		<div class="table-trends">
    				<table class="bordered">
					  <thead>
					    <tr>
					      <th>Customer NAme</th>
					      <th>Values(in INR)</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>James</td>
					      <td>5456</td>
					    </tr>
					    <tr>
					      <td>Andor</td>
					      <td>54654</td>
					    </tr>
					    <tr>
					      <td>Tamas</td>
					      <td>2134</td>
					    </tr>
					    <tr>
					      <td>Zoli</td>
					      <td>2132</td>
					    </tr>
					    <tr>
					      <td>Szabi</td>
					      <td>12134</td>
					    </tr>
					  </tbody>
					</table>
    			</div>
    	</div>
    	<div class="clearfix"></div>
    </div>
    <!-- export chart -->
    <div class="export-chart">
    	<div class="col-md-6 top-padding">
    		<h5>Suppliers form origin country</h5>
    		<div class="data-style">
    			<div id="suppliers-origin"></div>	
    		</div>
    		
    	</div>
    	<div class="col-md-6 top-padding">
    		<h5>Worldwide buyers growth trends</h5>
    		<div class="data-style">
    			<div id="worldwide-trends"></div>	
    		</div>
    	</div>
    	<div class="clearfix"></div>
    </div>
    <!-- data table -->
    <div class="data-table">
    	<h4 class="head">Top 10 suppliers from origin country</h4>
    	<table class="bordered">
		  <thead>
		    <tr>
		      <th>Origin </th>
		      <th>Total Amount</th>
		      <th>Market Share</th>
		      <th>Quantity</th>
		      <th>Quantity(in %)</th>
		      <th>Frequency</th>
		      <th>Frequency(in %)</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>Hongkong <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>2.7M</td>
		      <td>35.72 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>12523.1</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>South	Korea <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5.9M</td>
		      <td>25.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>12315.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Vietnam <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>4.5M</td>
		      <td>23.25 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>2502.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Ireland <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>1.1M</td>
		      <td>66.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i> </td>
		      <td>52312.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Finland <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>6.0M</td>
		      <td>55.25<i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5465.25</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>12134</td>
		    </tr>
		    
		  </tbody>
		</table>
    </div>

    <!-- data table -->
    <div class="data-table">
    	<h4 class="head">Top 10 buyers worldwide</h4>
    	<table class="bordered">
		  <thead>
		    <tr>
		      <th>Origin </th>
		      <th>Total Amount</th>
		      <th>Market Share</th>
		      <th>Quantity</th>
		      <th>Quantity(in %)</th>
		      <th>Frequency</th>
		      <th>Frequency(in %)</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>Hongkong <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>2.7M</td>
		      <td>35.72 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>12523.1</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>South	Korea <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5.9M</td>
		      <td>25.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>12315.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Vietnam <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>4.5M</td>
		      <td>23.25 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>2502.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Ireland <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>1.1M</td>
		      <td>66.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i> </td>
		      <td>52312.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Finland <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>6.0M</td>
		      <td>55.25<i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5465.25</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>12134</td>
		    </tr>
		    
		  </tbody>
		</table>
    </div>
    
    <!-- section ports -->
    <!-- export chart -->
    <div class="export-chart">
    	<div class="col-md-6 top-padding">
    		<h5>Port of export from origin country</h5>
    		<div class="data-style">
    			<img style="margin:0 auto;" src="/assets/images/map1.jpg" class="img-responsive" alt="">
    		</div>
    		
    	</div>
    	<div class="col-md-6 top-padding">
    		<h5>Countries of export</h5>
    		<div class="data-style" style="height: 215px;overflow: hidden;">
    			<img style="margin:0 auto; width: 100%;" src="/assets/images/map2.jpg" class="img-responsive" alt="">
    		</div>
    	</div>
    	<div class="clearfix"></div>
    </div>
    
  </div>
</div>
@endsection

@section('scripts')
<script src="/assets/js/buyers.js"></script>
@endsection