@extends('app')
@section('main-content')
<div class="tabs">
  <ul class="tabs-nav">
    <li><a href="/">Overall data</a></li>
    <li class="tab-active"><a href="/country-specific-data">Country Specific data</a></li>
    <li><a href="#">Commodityspecific data</a></li>
    <li><a href="/buyers-data">Buyers Insights</a></li>
  </ul>
  <div class="tabs-stage">
  	<!-- select target data -->
  	<div class="target-data">
  		<h4 class="heading">Select Target Countries</h4>
		<div class="searchboxwrapper">
			<input class="searchbox" type="text" value="" name="s"  id="s">
			<input class="searchsubmit" type="submit" id="searchsubmit" value="">
		</div>
  	</div>
    <!-- export chart -->
    <div class="export-chart">
    	<div class="col-md-6 top-padding">
    		<h5>Export trends of targeted countries</h5>
    		<div class="data-style">
    			<div id="traget-countries"></div>	
    		</div>
    		
    	</div>
    	<div class="col-md-6 top-padding">
    		<h5>Market share in targeted countries</h5>
    		<div class="data-style">
    			<div class="select-chart pull-left">
    				<h5>Country-wise Market share</h5>
    			</div>
	    		<canvas id="country-target" width="400" height="200"></canvas>	
    		</div>
    	</div>
    	<div class="clearfix"></div>
    </div>
    <!-- data table -->
    <div class="data-table">
    	<h4 class="head">Origin countries and markets share in -
			<select name="slct" id="slct">
			    <option value="1">Argentina</option>
			    <option value="2">China</option>
			 </select>	
    	</h4>
    	<table class="bordered">
		  <thead>
		    <tr>
		      <th>Origin </th>
		      <th>Total Amount</th>
		      <th>Market Share</th>
		      <th>Quantity</th>
		      <th>Quantity(in %)</th>
		      <th>Frequency</th>
		      <th>Frequency(in %)</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>Hongkong <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>2.7M</td>
		      <td>35.72 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>12523.1</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>South	Korea <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5.9M</td>
		      <td>25.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>12315.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Vietnam <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>4.5M</td>
		      <td>23.25 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>2502.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Ireland <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>1.1M</td>
		      <td>66.25 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i> </td>
		      <td>52312.2</td>
		      <td>51 <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>16.54</td>
		    </tr>
		    <tr>
		      <td>Finland <i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>6.0M</td>
		      <td>55.25<i class="green-arrow pull-right fa fa-arrow-up" aria-hidden="true"></i></td>
		      <td>5465.25</td>
		      <td>51 <i class="red-arrow pull-right fa fa-arrow-down" aria-hidden="true"></i></td>
		      <td>9.561</td>
		      <td>12134</td>
		    </tr>
		    
		  </tbody>
		</table>
    </div>
    
    <!-- section ports -->
    <div class="ports">
    	<h4 class="head" style="margin-bottom: 11px;">Port-wise Trends for -
			<select name="slct" id="slct">
			    <option value="1">Argentina</option>
			    <option value="2">China</option>
			 </select>	
    	</h4>
    	<div class="port">
    		<div class="col-md-6 top-padding">
    			<div class="table-trends">
    				<div class="data-style">
    					<div id="portchart"></div>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-6 top-padding">
    			<div class="table-trends">
    				<table class="bordered">
					  <thead>
					    <tr>
					      <th>Customer NAme</th>
					      <th>Values(in INR)</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>James</td>
					      <td>5456</td>
					    </tr>
					    <tr>
					      <td>Andor</td>
					      <td>54654</td>
					    </tr>
					    <tr>
					      <td>Tamas</td>
					      <td>2134</td>
					    </tr>
					    <tr>
					      <td>Zoli</td>
					      <td>2132</td>
					    </tr>
					    <tr>
					      <td>Szabi</td>
					      <td>12134</td>
					    </tr>
					  </tbody>
					</table>
    			</div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="/assets/js/country.js"></script>
@endsection