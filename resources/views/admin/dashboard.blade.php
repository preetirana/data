@extends('app')
@section('main-content')
<div class="tabs">
  <ul class="tabs-nav">
    <li class="tab-active"><a href="/">Overall data</a></li>
    <li><a href="/country-specific-data">Country Specific data</a></li>
    <li><a href="#">Commodityspecific data</a></li>
    <li><a href="/buyers-data">Buyers Insights</a></li>
  </ul>
  <div class="tabs-stage">
  	<!-- overall data -->
    <div class="overall-data">
      <h4 class="head">Export Trends</h4>
      <div class="export-values">
      	<div class="col-md-3 top-padding">
      		<div class="value">
      			<span>1006</span>
      			<h5>Commodities</h5>
      		</div>
      	</div>
      	<div class="col-md-3 top-padding">
      		<div class="value">
      			<span>Apr 2018</span>
      			<h5>Starting form</h5>
      		</div>
      	</div>
      	<div class="col-md-3 top-padding">
      		<div class="value">
      			<span>600,784.13</span>
      			<h5>Import value</h5>
      		</div>
      	</div>
      	<div class="col-md-3 top-padding">
      		<div class="value">
      			<span>408</span>
      			<h5>Ports</h5>
      		</div>
      	</div>
      	<div class="clearfix"></div>
      </div>
    </div>
    <!-- export chart -->
    <div class="export-chart">
    	<div class="col-md-6 top-padding">
    		<h5>Export Over Time</h5>
    		<div class="data-style">
    			<div class="select-chart pull-right">
    				<select name="slct" id="slct">
					    <option value="1">Past 6 months</option>
					    <option value="2">Past 1 year</option>
					    <option value="3">Current Month</option>
					 </select>
    			</div>
	    		<canvas id="over-time"></canvas>	
    		</div>
    		
    	</div>
    	<div class="col-md-6 top-padding">
    		<h5>Export Over Countries (in INR)</h5>
    		<div class="data-style">
    			<div class="select-chart pull-right">
    				<select name="slct" id="slct">
					    <option value="1">Past 6 months</option>
					    <option value="2">Past 1 year</option>
					    <option value="3">Current Month</option>
					 </select>
    			</div>
    			<div class="clearfix"></div>
	    		<img style="margin:0 auto; width: 100%;" src="/assets/images/map.jpg" class="img-responsive" alt="">	
    		</div>
    		
    	</div>
    	<div class="clearfix"></div>
    </div>
    <!-- data table -->
    <div class="data-table">
    	<table class="bordered">
		  <thead>
		    <tr>
		      <th>Date</th>
		      <th>Designation Port</th>
		      <th>Source Port</th>
		      <th>HS Code</th>
		      <th>Product Description</th>
		      <th>Commodities</th>
		      <th>Values(in INR)</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>James</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>@james</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>5456</td>
		    </tr>
		    <tr>
		      <td>Andor</td>
		      <td>Nagy</td>
		      <td>Designer</td>
		      <td>@andornagy</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>54654</td>
		    </tr>
		    <tr>
		      <td>Tamas</td>
		      <td>Biro</td>
		      <td>Game Tester</td>
		      <td>@tamas</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>2134</td>
		    </tr>
		    <tr>
		      <td>Zoli</td>
		      <td>Mastah</td>
		      <td>Developer</td>
		      <td>@zoli</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>2132</td>
		    </tr>
		    <tr>
		      <td>Szabi</td>
		      <td>Nagy</td>
		      <td>Chief Sandwich Eater</td>
		      <td>@szabi</td>
		      <td>Matman</td>
		      <td>Chief Sandwich Eater</td>
		      <td>12134</td>
		    </tr>
		    
		    <tr>
		      <td colspan="7">
		      	<div class="read-more"><a href="#">View more</a></div>
		      </td>
		    </tr>
		  </tbody>
		</table>
    </div>
    <!-- top import trends -->
    <div class="trends">
    	<h4 class="head">Top 10 Global Import Trends for -
			<select name="slct" id="slct">
			    <option value="1">Shoes</option>
			    <option value="2">Bags</option>
			    <option value="3">Phones</option>
			 </select>	
    	</h4>

    	<h4 class="new-heading">India export value for <span>shoes</span> during June 2018-August 2018 is <span class="red">21CR</span> INR </h4>

    	<div class="section-trends">
    		<div class="col-md-6 top-padding">
    			<div class="table-trends">
	    			<table class="bordered">
					  <thead>
					    <tr>
					      <th>Date</th>
					      <th>Importing Country</th>
					      <th>HS Code</th>
					      <th>Values(in INR)</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>James</td>
					      <td>Matman</td>
					      <td>Chief Sandwich Eater</td>
					      <td>5456</td>
					    </tr>
					    <tr>
					      <td>Andor</td>
					      <td>Nagy</td>
					      <td>Chief Sandwich Eater</td>
					      <td>54654</td>
					    </tr>
					    <tr>
					      <td>Tamas</td>
					      <td>Biro</td>
					      <td>Chief Sandwich Eater</td>
					      <td>2134</td>
					    </tr>
					    <tr>
					      <td>Zoli</td>
					      <td>Mastah</td>
					      <td>Chief Sandwich Eater</td>
					      <td>2132</td>
					    </tr>
					    <tr>
					      <td>Szabi</td>
					      <td>Nagy</td>
					      <td>Chief Sandwich Eater</td>
					      <td>12134</td>
					    </tr>
					  </tbody>
					</table>	
    			</div>
    		</div>
    		<div class="col-md-6 top-padding">
    			<div class="data-style">
    			<div class="select-chart pull-left">
    				<h5>Country-wise Market share</h5>
    			</div>
	    		<canvas id="country-wise" width="400" height="120"></canvas>	
    		</div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    </div>
    <!-- section ports -->
    <div class="ports">
    	<h4 class="head" style="margin-bottom: 11px;">Port-wise Trends for -
			<select name="slct" id="slct">
			    <option value="1">Shoes</option>
			    <option value="2">Bags</option>
			    <option value="3">Phones</option>
			 </select>	
    	</h4>
    	<div class="port">
    		<div class="col-md-6 top-padding">
    			<div class="table-trends">
    				<div class="data-style">
    					<div id="portchart"></div>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-6 top-padding">
    			<div class="table-trends">
    				<table class="bordered">
					  <thead>
					    <tr>
					      <th>Customer NAme</th>
					      <th>Values(in INR)</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>James</td>
					      <td>5456</td>
					    </tr>
					    <tr>
					      <td>Andor</td>
					      <td>54654</td>
					    </tr>
					    <tr>
					      <td>Tamas</td>
					      <td>2134</td>
					    </tr>
					    <tr>
					      <td>Zoli</td>
					      <td>2132</td>
					    </tr>
					    <tr>
					      <td>Szabi</td>
					      <td>12134</td>
					    </tr>
					  </tbody>
					</table>
    			</div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="/assets/js/overall.js"></script>
@endsection