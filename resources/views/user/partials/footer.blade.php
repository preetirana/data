<footer>	
	<div class="container">	
		<div class="row">	
			<div class="col-md-3">
				<img style="width:130px;" src="/website/assets/images/white-logo.png" alt="">
				<div class="address">
					<div class="col-md-2">
						<img src="/website/assets/images/address.png" alt="">
					</div>
					<div class="col-md-10">	
						<p>#15, 24th Main Road, JP Nagar 7th Phase Bangalore, IN 560078</p>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<h4>PRODUCT</h4>
				<ul>
					<li><a href="#">Search</a></li>
					<li><a href="#">Digital Services</a></li>
					<li><a href="#">Request a Demo</a></li>
					<li><a href="#">Countries</a></li>
				</ul>
			</div>
			<div class="col-md-2">
				<h4>BROWSE</h4>
				<ul>
					<li><a href="#">Importers</a></li>
					<li><a href="#">Exporters</a></li>
					<li><a href="#">Ports</a></li>
					<li><a href="#">HS Codes</a></li>
				</ul>
			</div>
			<div class="col-md-2">
				<h4>QUICK LINKS</h4>
				<ul>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Blogs</a></li>
					<li><a href="#">Terms</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<div class="social-links">
					<div class="col-md-2"><i class="fa fa-mobile" aria-hidden="true"></i></div>
					<div class="col-md-10"><a href="#">+91 9090909090</a></div>
					<div class="clearfix"></div>
				</div>
				<div class="social-links">
					<div class="col-md-2"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
					<div class="col-md-10"><a href="#">hello@tradestrings.com</a></div>
					<div class="clearfix"></div>
				</div>
				<h3>Follow us on</h3>
				<div class="follow">
					<div class="col-md-3"><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></div>
					<div class="col-md-3"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
					<div class="col-md-3"><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>
<div class="bottom-footer">	
	<p><i class="fa fa-copyright" aria-hidden="true"></i> 2019 Trade Strings. All rights reserved.</p>
	<div class="go-to-top">
		<i class="fa fa-angle-up" aria-hidden="true"></i>
	</div>
</div>