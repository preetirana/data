@extends('user.home-app')
@section('main-content')
<div class="container-fluid">
	<div class="row">
		<div class="banner-home">
			<div class="overlay-custom">
				<div class="col-md-8 col-md-offset-2">
					<div class="overlay-content">
						<h3>Search import export data worldwide for market research, generate report on curated data, find buyers & suppliers and more...</h3>	

						<div class="import">
							<form action="">
								<div class="col-md-2">
									<div class="top-import">
										<label for="country">Select Contry</label>
										<select name="" id="" class=" form-control">
											<option value="India">India</option>
											<option value="USA">USA</option>
											<option value="Japan">Japan</option>
											<option value="China">China</option>
										</select>	
									</div>
									
								</div>
								<div class="col-md-8">
									<div class="top-import">
										<label for="search">Search Product description/HS Code/Import...</label>
										<input type="text" class="form-control">	
									</div>
									
								</div>
								<div class="col-md-2 no-padding">
									<button type="submit">Import <i class="fa fa-angle-down" aria-hidden="true"></i></button>
								</div>
								<div class="clearfix"></div>
							</form>
						</div>
					</div>
						
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- how tradestrings works -->

<div class="tradestrings">
	<div class="container">
		<h3 class="heading">How tradestrings works..!</h3>
		<p>we collect data from reliable data source around the world, we help you keep track of your competitors, our data points help you gain insights like never before</p>
		<div class="row">
			<div class="trades">
				<div class="col-md-4">
					<div class="trade">
						<div class="col-md-3">
							<img src="/website/assets/images/heart.png" alt="">	
						</div>
						<div class="col-md-9">
							<h4>Passion</h4>
							<p>The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="trade">
						<div class="col-md-3">
							<img src="/website/assets/images/location.png" alt="">	
						</div>
						<div class="col-md-9">
							<h4>Creativity</h4>
							<p>The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="trade">
						<div class="col-md-3">
							<img src="/website/assets/images/star.png" alt="">	
						</div>
						<div class="col-md-9">
							<h4>Quality</h4>
							<p>The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>	
			</div>
			
		</div>
	</div>
</div>

<!-- end of how tradestrings works -->

<!-- discover -->
<div class="discover-new">
	<div class="container">
		<h3 class="heading">Discover new business opportunities worldwide</h3>
	</div>
</div>
<!-- end of discover -->

<!-- digitise -->
<div class="digitise-new">
	<div class="container">
		<h3 class="heading">digitise your business with us</h3>
		<div class="digitise">
			<div class="col-md-6">
				<img src="/website/assets/images/image.png" alt="" class="img-responsive">
			</div>
			<div class="col-md-6">
				<div class="content">
					<p>We will create your business identity online. Our team of experts will handle your digital business from buying the domain to getting you leads online on regular  basis.  Our team of experts will handle your digital business from buying the domain to getting you leads online on regular  basis</p>
					<div class="buttons-custom">
						<button class="black">Check Our Subscriptions</button> <button class="black white">Talk to us</button>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- end of digitise -->

<!-- sounds interesting -->
<div class="interesting">
	<div class="overlay-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="content">
					<h2>Sounds Interesting?</h2>
					<h6>Take the Next Steps</h6>	
				</div>
			</div>
			<div class="col-md-6">
				<div class="buttons-custom">
					<button class="black">Request a Demo</button> <button class="black white">Contact Us</button>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- end of sounds interesting -->


@endsection

@section('scripts')
<script>
	$(document).ready(function() {
	    $('.js-example-basic-single').select2({
	    	minimumResultsForSearch: -1
	    });
	});
</script>
@endsection